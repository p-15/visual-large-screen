import { createApp } from "vue";

import App from "./App.vue";
import "normalize.css";
import "./styles/global.scss";
import "./styles/variables.css";

createApp(App).mount("#app");
